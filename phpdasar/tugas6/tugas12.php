<?php
include "fungsi.php";
$siswas = [
    ["Budi","2000-01-31","Pria","60"],
    ["Ani","2000-03-31","Wanita","90"],
    ["Sembarang","2000-02-29","Pria","100"],
    ["Ridho","2000-01-03","Pria","80"]
];

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data siswa</title>
</head>
<body>
    <table border="1" cellspacing="0">
        <tr>
            <th>No</th>
            <th>Nama Siswa/Siswi</th>
            <th>Tanggal Lahir</th>
            <th>Jenis Kelamin</th>
            <th>Nilai</th>
            <th>Nilai Huruf</th>
        </tr>
        <?php foreach ($siswas as $key => $siswa) {?>
        <tr>
            <td><?= $keys = $key + 1; ?></td>
            <?php foreach ($siswa as $index => $tampil) {?>
            <td><?= $tampil ?></td>
            <?php } ?>
            <td><?= nilaiHuruf($siswas[$key][3]) ?></td>
        </tr>
        <?php } ?>
    </table>
</body>
</html>