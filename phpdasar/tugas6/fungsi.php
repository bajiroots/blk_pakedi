<?php

// funsi merubah hari dalam bahasa inggris ke indonesia
function hariIngtoInd($hari)
{
    $hariLow = strtolower($hari);
    if ($hariLow == "monday") {
        return "$hari => Senin <br>";
    }elseif ($hariLow == "tuesday"){
        return "$hari => Selasa <br>";
    }elseif ($hariLow == "wednesday"){
        return "$hari => Rabu";
    }elseif ($hariLow == "thursday"){
        return "$hari => Kamis <br>";
    }elseif ($hariLow == "friday"){
        return "$hari => Jum'at <br>";
    }elseif ($hariLow == "saturday"){
        return "$hari => Sabtu <br>";
    }elseif ($hariLow == "sunday"){
        return "$hari => Minggu <br>";
    }else{
        return "$hari bukan hari <br>";
    }
}



// fungsi untuk merubah bulan inggris ke indonesia
function bulanIngtoInd($bulan)
{
    $bulanLow = strtolower($bulan);
    if ($bulanLow == "january") {
        return "$bulan => Januari <br>";
    }elseif ($bulanLow == "february"){
        return "$bulan => Februari <br>";
    }elseif ($bulanLow == "march"){
        return "$bulan => Maret <br>";
    }elseif ($bulanLow == "april"){
        return "$bulan => April <br>";
    }elseif ($bulanLow == "may"){
        return "$bulan => Mei <br>";
    }elseif ($bulanLow == "june"){
        return "$bulan => Juni <br>";
    }elseif ($bulanLow == "july"){
        return "$bulan => Juli <br>";
    }elseif ($bulanLow == "august"){
        return "$bulan => Agustus <br>";
    }elseif ($bulanLow == "september"){
        return "$bulan => September <br>";
    }elseif ($bulanLow == "october"){
        return "$bulan => Oktober <br>";
    }elseif ($bulanLow == "november"){
        return "$bulan => November <br>";
    }elseif ($bulanLow == "december"){
        return "$bulan => December <br>";
    }else{
        return "$bulan bukan bulan <br>";
    }
}


// fungsi untuk merubah format tanggal inggris ke indonesia 2020-12-31 --> 31-12-2002
function tanggalIngtoInd($tanggal)
{
    // $tanggalStr = (string)$tanggal;
    $array = explode("-",$tanggal);
    switch ($array[1]) {
        case '12':$array[1] = "Desember";break;
        case '11':$array[1] = "November";break;
        case '10':$array[1] = "Oktober";break;
        case '9':$array[1] = "September";break;
        case '8':$array[1] = "Agustus";break;
        case '7':$array[1] = "Juli";break;
        case '6':$array[1] = "Juni";break;
        case '5':$array[1] = "Mei";break;
        case '4':$array[1] = "April";break;
        case '3':$array[1] = "Maret";break;
        case '2':$array[1] = "Februari";break;
        case '1':$array[1] = "Januari";break;
        default:return "anda tidak memasukkan bulan dengan tepat";break;
    }
    return $array[2].'-'.$array[1].'-'.$array[0].'<br>';
    return var_dump($array);
}



// fungsi tanggal indonesia ke inggris
function tanggalIndtoIng($tanggal)
{
    $array = explode("-",$tanggal);
    $bulan = strtolower($array[1]);
    switch ($bulan) {
        case 'desember':$bulan = "12";break;
        case 'november':$bulan = "11";break;
        case 'october':$bulan = "10";break;
        case 'september':$bulan = "9";break;
        case 'agustus':$bulan = "8";break;
        case 'juli':$bulan = "7";break;
        case 'juni':$bulan = "6";break;
        case 'mei':$bulan = "5";break;
        case 'april':$bulan = "4";break;
        case 'maret':$bulan = "3";break;
        case 'februari':$bulan = "2";break;
        case 'januari':$bulan = "1";break;
        default:return "anda tidak memasukkan bulan dengan tepat";break;
    }
    return $array[2].'-'.$bulan.'-'.$array[0];
    // return var_dump($array);
}


$tgl = "2020-12-08";
function balik($tgl)
{
    return date("d-m-Y",strtotime($tgl));
}

//versi penndel
$tgl ="2020-12-08";

// fungsi hitung nilai huruf
function nilaiHuruf($nilai)
    {
        if ($nilai >= 80) {
            return "A <br>";
        }elseif ($nilai < 80 && $nilai >= 70 ) {
            return "B <br>";
        }elseif ($nilai < 70 && $nilai >= 60 ) {
            return "C <br>";
        }elseif ($nilai < 60 && $nilai >= 50 ) {
            return "D <br>";
        }else{
            return "E <br>";
        }
    }