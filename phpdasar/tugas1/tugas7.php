<?php

// Nilai Semester 1
    $nilaiIpa = 84;
    $nilaiIps = 72;
    $nilaiMtk = 86;
    $nilaiBhsIng = 91;
    $nilaiBhsInd = 77;

// Rata-rata nilai semester 1
    $jumlahNilai = ($nilaiIpa + $nilaiIps + $nilaiBhsInd + $nilaiBhsIng + $nilaiMtk) / 5;

// Nilai Semester 2
    $nilaiIpas2 = 70;
    $nilaiIpss2 = 59;
    $nilaiMtks2 = 76;
    $nilaiBhsIngs2 = 61;
    $nilaiBhsInds2 = 77;

// Rata-rata nilai semester 2
    $jumlahNilais2 = ($nilaiIpas2 + $nilaiIpss2 + $nilaiBhsInds2 + $nilaiBhsIngs2 + $nilaiMtks2) / 5;

// Jumlah Nilai semester 1
    $jumlah1 = $nilaiIpa + $nilaiIps + $nilaiBhsInd + $nilaiBhsIng + $nilaiMtk;

// Jumlah Nilai semester 2
    $jumlah2 = $nilaiIpas2 + $nilaiIpss2 + $nilaiBhsInds2 + $nilaiBhsIngs2 + $nilaiMtks2;

// ternary lulus atau tidak lulus
    $jmlhNilai = ($jumlahNilai >= 70) ? "Lulus" : "Tidak Lulus";
    $jmlhNilai2 = ($jumlahNilais2 >= 70) ? "Lulus" : "Tidak Lulus";

// ternary warna tulisan
    $warnaNilai = ($jumlahNilai >= 70) ? "limegreen" : "red";
    $warnaNilai2 = ($jumlahNilais2 >= 70) ? "limegreen" : "red";

// Function untuk menentukan predikat
    function hitungNilai($nilai){
        switch ($nilai){
            case ($nilai >= 80):
                echo "Nilai A";
                break;
            case ($nilai < 80 && $nilai >= 70 ):
                echo "Nilai B";
                break;
            case ($nilai < 70 && $nilai >= 60):
                echo "Nilai C";
                break;
            case ($nilai < 60 && $nilai >= 50 ):
                echo "Nilai D";
                break;
            default:
                echo "Nilai E";
                break;
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .jumlah,.nilai{
            text-align: center;
        }
        table{
            width: 500px;
            height: 200px;
        }
        table tr td{
            padding: 10px;
        }
        table tr th{
            height: 40px;
        }
        tr:nth-child(even){
            background-color: antiquewhite;
        }
        tr:nth-child(odd){
            background-color: aqua;
        }
    </style>
</head>
<body>
    <table border="0px">
        <tr class="head">
            <th rowspan="2" >Mata Pelajaran</th>
            <th colspan="2">Semester 1</th>
            <th colspan="2">Semester 2</th>
        </tr>


        <tr class="head">
            <th>Angka</th>
            <th>Huruf</th>
            <th>Angka</th>
            <th>Huruf</th>
        </tr>


        <tr>
            <td>IPA</td>
            <td class="nilai"> <?php echo $nilaiIpa ?> </td>
            <td class="nilai"> <?php echo hitungNilai($nilaiIpa)  ?> </td>
            <td class="nilai"> <?php echo $nilaiIpas2 ?> </td>
            <td class="nilai"> <?php echo hitungNilai($nilaiIpas2)  ?> </td>
        </tr>


        <tr>
            <td>IPS</td>
            <td class="nilai"> <?php echo $nilaiIps ?> </td>
            <td class="nilai"> <?php echo hitungNilai($nilaiIps) ?> </td>
            <td class="nilai"> <?php echo $nilaiIpss2 ?> </td>
            <td class="nilai"> <?php echo hitungNilai($nilaiIpss2) ?> </td>
        </tr>


        <tr>
            <td>MTK</td>
            <td class="nilai"> <?php echo $nilaiMtk ?> </td>
            <td class="nilai"> <?php echo hitungNilai($nilaiMtk)  ?> </td>
            <td class="nilai"> <?php echo $nilaiMtks2 ?> </td>
            <td class="nilai"> <?php echo hitungNilai($nilaiMtks2)  ?> </td>
        </tr>


        <tr>
            <td>Bhs Ind</td>
            <td class="nilai"> <?php echo $nilaiBhsInd ?> </td>
            <td class="nilai"> <?php echo hitungNilai($nilaiBhsInd)  ?> </td>
            <td class="nilai"> <?php echo $nilaiBhsInds2 ?> </td>
            <td class="nilai"> <?php echo hitungNilai($nilaiBhsInds2)  ?> </td>
        </tr>


        <tr>
            <td>Bhs Ing</td>
            <td class="nilai"> <?php echo $nilaiBhsIng ?> </td>
            <td class="nilai"> <?php echo hitungNilai($nilaiBhsIng)  ?> </td>
            <td class="nilai"> <?php echo $nilaiBhsIngs2 ?> </td>
            <td class="nilai"> <?php echo hitungNilai($nilaiBhsIngs2)  ?> </td>
        </tr>


        <tr>
            <th>Jumlah</th>
            <td colspan="2" class="jumlah"><?php echo $jumlah1 ?></td>
            <td colspan="2" class="jumlah"><?php echo $jumlah2 ?></td>
        </tr>


        <tr>
            <th>Rata-Rata</th>
            <td colspan="2" class="jumlah"><?php echo $jumlahNilai ?></td>
            <td colspan="2" class="jumlah"><?php echo $jumlahNilais2 ?></td>
        </tr>


        <tr>
            <th>Status</th>
            <td colspan="2" class="jumlah" style=" font-weight: bolder; color: <?php echo $warnaNilai ?>;"><?php echo $jmlhNilai  ?></td>
            <td colspan="2" class="jumlah" style=" font-weight: bolder; color: <?php echo $warnaNilai2 ?>;"><?php echo $jmlhNilai2 ?></td>
        </tr>
    </table>

</body>
</html>