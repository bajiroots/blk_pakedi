<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        tr:nth-child(even){
            background-color: aquamarine;
        }
        tr:nth-child(odd){
            background-color: white ;
        }
        table .head{
            background: aqua;
        }
    </style>
</head>

<body>
    <?php 
        // echo "hello world ! ";
        // $nama= "Dicky";
        // $nama= "ridho"; 
        // echo $nama."<br>";
        // $nilai=80;
        // $ratarata=80.10;
        // var_dump($ratarata) ."<br>";
        // var_dump($nilai);
        // $h=30;
        // echo "$h+20";
        // echo "<br>";
        // echo $h+20;
        // echo "<br>";
        // echo $h++;

        $nilaiIpa = 84;
        $nilaiIps = 72;
        $nilaiMtk = 86;
        $nilaiBhsIng = 91;
        $nilaiBhsInd = 77;
        $jumlahNilai = ($nilaiIpa+$nilaiIps+$nilaiBhsInd+$nilaiBhsIng+$nilaiMtk)/5;

        $nilaiIpas2 = 70;
        $nilaiIpss2 = 79;
        $nilaiMtks2 = 76;
        $nilaiBhsIngs2 = 71;
        $nilaiBhsInds2 = 77;
        $jumlahNilais2 = ($nilaiIpas2+$nilaiIpss2+$nilaiBhsInds2+$nilaiBhsIngs2+$nilaiMtks2)/5;
?>
    <table>

        <tr class="head">
            <th>Mata Pelajaran</th>
            <th>Semester1</th>
            <th>Semester2</th>
        </tr>


        <tr>
            <td>Ipa</td>
            <td><?php echo $nilaiIpa ?></td>
            <td><?php echo $nilaiIpas2 ?></td>
        </tr>


        <tr>
            <td>Ips</td>
            <td><?php echo $nilaiIps ?></td>
            <td><?php echo $nilaiIpss2 ?></td>
        </tr>


        <tr>
            <td>Mtk</td>
            <td><?php echo $nilaiMtk ?></td>
            <td><?php echo $nilaiMtks2 ?></td>
        </tr>


        <tr>
            <td>Bhs Ind</td>
            <td><?php echo $nilaiBhsInd ?></td>
            <td><?php echo $nilaiBhsInds2 ?></td>
        </tr>


        <tr>
            <td>Bhs Ing</td>
            <td><?php echo $nilaiBhsIng ?></td>
            <td><?php echo $nilaiBhsIngs2 ?></td>
        </tr>


        <tr>
            <td>Rata-Rata</td>
            <td><?php echo $jumlahNilai ?></td>
            <td><?php echo $jumlahNilais2 ?></td>
        </tr>
    </table>
</body>

</html>